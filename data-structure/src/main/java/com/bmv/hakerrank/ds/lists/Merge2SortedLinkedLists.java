package com.bmv.hakerrank.ds.lists;

/**
 * You’re given the pointer to the head nodes of two sorted linked lists.
 * The data in both lists will be sorted in ascending order.
 * Change the next pointers to obtain a single, merged linked list which also has data in ascending order.
 * Either head pointer given may be null meaning that the corresponding list is empty.
 */
public class Merge2SortedLinkedLists {
    public static void main(String[] args) {
        Merge2SortedLinkedLists sortedLinkedLists = new Merge2SortedLinkedLists();
        Node f = new Node(1);
        Node s = new Node(2);
        Node mergeLists = sortedLinkedLists.mergeLists(f, s);

        f = new Node(2);
        s = new Node(1);
        mergeLists = sortedLinkedLists.mergeLists(f, s);

        f = null;
        s = new Node(1);
        mergeLists = sortedLinkedLists.mergeLists(f, s);

        f = new Node(2);
        s = null;
        mergeLists = sortedLinkedLists.mergeLists(f, s);
    }

    public Node mergeLists(Node headA, Node headB){
        if (headA == null) return headB;
        if (headB == null) return headA;
        Node result = headA;
        Node prev = null;
        while (headA != null && headB != null){
            if (headA.getData() > headB.getData()){
                if (prev == null){
                    prev = headB;
                    result = prev;
                    headB = headB.getNext();
                    prev.setNext(headA);
                }
                else {
                    prev.setNext(headB);
                    headB = headB.getNext();
                    prev.getNext().setNext(headA);
                }
            }
            else {
                prev = headA;
                headA = headA.getNext();
            }

        }
        if (headA == null){
            prev.setNext(headB);
        }
        return result;
    }
}
