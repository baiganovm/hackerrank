package com.bmv.hakerrank.ds.lists;

/**
 * A linked list is said to contain a cycle if any node is visited more than once while traversing the list.

 Complete the function provided for you in your editor.
 It has one parameter: a pointer to a Node object named that points to the head of a linked list.
 Your function must return a boolean denoting whether or not there is a cycle in the list.
 If there is a cycle, return true; otherwise, return false.

 0<= list size <= 100
 */
public class CycleDetection {
    public static void main(String[] args) {
        CycleDetection cycleDetection = new CycleDetection();
        Node node = new Node(3);
        Node next = new Node(2);
        next.setNext(node);
        node.setNext(next);
        Node h = new Node(1);
        h.setNext(next);

        System.out.println(cycleDetection.hasCycle(h));
    }


    public boolean hasCycle(Node head) {
        if (head == null || head.getNext() == null) return false;
        Node first = head;
        Node quicker = head.getNext();
        while (first != null && quicker != null && quicker.getNext() != null){
            if (first == quicker) return true;
            first = first.getNext();
            quicker = quicker.getNext().getNext();
        }
        return false;
    }
}
