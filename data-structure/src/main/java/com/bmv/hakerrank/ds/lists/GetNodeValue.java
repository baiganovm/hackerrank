package com.bmv.hakerrank.ds.lists;

/**
 * You’re given the pointer to the head node of a linked list and a specific position.
 * Counting backwards from the tail node of the linked list, get the value of the node at the given position.
 * A position of 0 corresponds to the tail, 1 corresponds to the node before the tail and so on.
 */
public class GetNodeValue {
    public static void main(String[] args) {
        Node node = new Node(1);
        Node s = new Node(2);
        node.setNext(s);
        Node t = new Node(3);
        s.setNext(t);
        s = t;
        t = new Node(4);
        s.setNext(t);
        GetNodeValue getNodeValue = new GetNodeValue();
        int res = getNodeValue.getNode(node, 0);
        System.out.println(res == 4);
        res = getNodeValue.getNode(node, 1);
        System.out.println(res == 3);
        res = getNodeValue.getNode(node, 3);
        System.out.println(res == 1);
        res = getNodeValue.getNode(node, 4);
        System.out.println(res == -1);
    }

    public int getNode(Node head, int tailPos){
        Node iter = head;
        while (iter != null && tailPos > 0){
            iter = iter.getNext();
            tailPos--;
        }
        if (tailPos > 0 || iter == null) return -1;
        while(iter.getNext()!=null){
            head = head.getNext();
            iter = iter.getNext();
        }
        return head == null ? -1 : head.getData();
    }
}
