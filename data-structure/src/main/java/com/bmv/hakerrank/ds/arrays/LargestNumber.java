package com.bmv.hakerrank.ds.arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LargestNumber {

    public static void main(String[] args) {
        LargestNumber number = new LargestNumber();
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(3);
        a.add(30);
        a.add(9);
        a.add(34);
        a.add(5);
        a.add(12);
        a.add(121);
        System.out.println(number.largestNumber(a));
    }

    public String largestNumber(final List<Integer> A) {
        StringBuilder sb = new StringBuilder();
        List<String> str = new ArrayList<String>();
        for (Integer integer : A) {
            str.add(integer.toString());
        }

        Collections.sort(str, new Comparator<String>() {
            public int compare(String o1, String o2) {
                String var1 = o1+o2;
                String var2 = o2+o1;
                return -1 * var1.compareTo(var2);
            }
        });
        if ("0".equals(str.get(0))) return "0";
        for (String s : str) {
            sb.append(s);
        }

        return sb.toString();
    }
}
