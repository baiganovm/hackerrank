package com.bmv.hakerrank.ds.stacks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EqualStacks {
    public static void main(String[] args) {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {
            String[] s = in.readLine().split(" ");
            int n1 = Integer.parseInt(s[0]);
            int n2 = Integer.parseInt(s[1]);
            int n3 = Integer.parseInt(s[2]);
            int h1[] = new int[n1];
            long sum1 = 0;
            s = in.readLine().split(" ");
            for (int h1_i = 0; h1_i < n1; h1_i++) {
                h1[h1_i] = Integer.parseInt(s[h1_i]);
                sum1 += h1[h1_i];
            }
            long sum2 = 0;
            int h2[] = new int[n2];
            s = in.readLine().split(" ");
            for (int h2_i = 0; h2_i < n2; h2_i++) {
                h2[h2_i] = Integer.parseInt(s[h2_i]);
                sum2 += h2[h2_i];
            }
            long sum3 = 0;
            int h3[] = new int[n3];
            s = in.readLine().split(" ");
            for (int h3_i = 0; h3_i < n3; h3_i++) {
                h3[h3_i] = Integer.parseInt(s[h3_i]);
                sum3 += h3[h3_i];
            }
            EqualStacks solver = new EqualStacks();
            System.out.println(solver.findEqualHeight(h1, h2, h3, sum1, sum2, sum3));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int findEqualHeight(int[] h1, int[] h2, int[] h3, long sum1, long sum2, long sum3) {
        if (sum1 == sum2 && sum2 == sum3) return (int) sum1;
        int lastIndex1 = 0;
        int lastIndex2 = 0;
        int lastIndex3 = 0;
        while (!(sum1 == sum2 && sum2 == sum3)){
            if (sum1 > sum2 || sum1 > sum3){
                sum1 -= h1[lastIndex1++];
            } else if (sum2 > sum1 || sum2 > sum3){
                sum2 -= h2[lastIndex2++];
            } else if (sum3 > sum1 || sum3 > sum2){
                sum3 -= h3[lastIndex3++];
            }
        }
        return (int) sum1;
    }
}
