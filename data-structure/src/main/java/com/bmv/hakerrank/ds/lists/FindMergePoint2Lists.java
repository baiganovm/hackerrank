package com.bmv.hakerrank.ds.lists;

/**
 * Given pointers to the head nodes of 2 linked lists that merge together at some point,
 * find the Node where the two lists merge.
 * It is guaranteed that the two head Nodes will be different, and neither will be NULL.
 * -----------
 * Possible solutions:
 * 1. One approach to use Set to save elements from one list.
 *    Iterate for each element from another list and check element exists in set or not. If exists, we find element.
 *    It would be work if equals of Node is oob implementation. Most likely equals method of Node will override and solution doesn't work.
 * 2. Calculate size of elements for both lists
 */
public class FindMergePoint2Lists {

    public static void main(String[] args) {
        Node f1 = new Node(1);
        Node f2 = new Node(2);
        Node s = new Node(3);
        Node s2 = new Node(4);

        f1.setNext(s);
        f2.setNext(s2);
        s2.setNext(s);
        FindMergePoint2Lists mergePoint2Lists = new FindMergePoint2Lists();
        Node point = mergePoint2Lists.findMergePoint(f1, f2);
        System.out.println(point == s);
    }

    public Node findMergePoint(Node headA, Node headB){
        if (headA == null || headB == null) return null;

        Node itA = headA;
        Node itB = headB;
        int sizeA = size(itA);
        int sizeB = size(itB);
        int diff = sizeA - sizeB;
        for (int i = 0; i < (diff > 0 ? diff :0); i++){
            itA = itA.getNext();
        }
        for (int i = 0; i < (diff < 0 ? Math.abs(diff) :0); i++){
            itB = itB.getNext();
        }

        while(itA != null && itB != null && itA != itB){
            itA = itA.getNext();
            itB = itB.getNext();
        }
        return itA;
    }

    private int size(Node head) {
        int size = 0;
        while (head != null){
            ++size;
            head = head.getNext();
        }
        return size;
    }
}
