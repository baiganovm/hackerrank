package com.bmv.hakerrank.ds.arrays;

public class VerifyPrimeNumber {
    public int isPrime(int a) {
        if (a == 1 || a == 0) return 0;
        for (int i = 2; i < a - 1; i++){
            if (a % i == 0) return 0;
        }
        return 1;
    }

}
