package com.bmv.hakerrank.ds.lists;

/**
 * You’re given the pointer to the head node of a sorted doubly linked list and an integer to insert into the list.
 * Create a node and insert it into the appropriate position in the list such that its order is maintained.
 * The head node might be NULL to indicate that the list is empty.
 */
public class InsertNodeIntoDoublyLinkedList {

    public static void main(String[] args) {
        InsertNodeIntoDoublyLinkedList service = new InsertNodeIntoDoublyLinkedList();
        Node head = service.insert(null, 1);
        head = service.insert(head, 5);
        head = service.insert(head, 3);

    }

    public Node insert(Node head,int data){
        if (head == null){
            head = new Node();
            head.setData(data);
            return head;
        }
        Node it = head;
        while (it.getNext() != null && it.getData() < data){
            it = it.getNext();
        }

        Node temp = new Node();
        temp.setData(data);
        if (it.getNext() == null && it.getData() < data){
            temp.setPrev(it);
            it.setNext(temp);
        }
        else{
            temp.setPrev(it.getPrev());
            temp.setNext(it);
            it.getPrev().setNext(temp);
            it.setPrev(temp);
        }
        return head;
    }
}
