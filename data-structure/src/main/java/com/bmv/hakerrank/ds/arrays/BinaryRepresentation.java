package com.bmv.hakerrank.ds.arrays;

public class BinaryRepresentation {

    public static void main(String[] args) {
        BinaryRepresentation binaryRepresentation
                = new BinaryRepresentation();
        System.out.println(binaryRepresentation.findDigitsInBinary(8));
    }


    public String findDigitsInBinary(int A) {
        StringBuilder res = new StringBuilder();
        while (A/2 > 0){
            res.insert(0, A % 2);
            A = A/2;
        }

        res.insert(0, A);

        return res.toString();
    }
}
