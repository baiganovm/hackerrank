package com.bmv.hakerrank.ds.trees;

/**
 * Created by main on 26.10.16.
 */
public class Node {
    private int data;
    private Node left;
    private Node right;
    private int level;

    public Node(int data) {
        this.data = data;
    }

    public Node() {
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
