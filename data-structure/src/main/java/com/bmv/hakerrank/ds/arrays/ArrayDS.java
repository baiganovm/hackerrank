package com.bmv.hakerrank.ds.arrays;

/**
 * Given an array a of n integers, print each element in reverse order as a single line of space-separated integers.
 * 1 <= n <= 10^3
 * 1 <= a(i) <= 10^4
 */
public class ArrayDS {
    public static void main(String[] args) {
        ArrayDS arrayDS = new ArrayDS();
        System.out.println(arrayDS.printReverseElements(new int[]{0,1,1,12,3,4,1000}));
        System.out.println(arrayDS.printReverseElements(new int[]{}));
        System.out.println(arrayDS.printReverseElements(new int[]{0,1,1}));
    }

    public String printReverseElements(int[] a){
        StringBuilder sb = new StringBuilder();
        for (int i = a.length-1; i >= 0; i --){
            sb.append(a[i]);
            if (i != 0) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }
}
