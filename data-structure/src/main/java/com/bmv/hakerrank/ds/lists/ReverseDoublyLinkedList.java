package com.bmv.hakerrank.ds.lists;

/**
 * You’re given the pointer to the head node of a doubly linked list.
 * Reverse the order of the nodes in the list.
 * The head node might be NULL to indicate that the list is empty.
 */
public class ReverseDoublyLinkedList {
    public static void main(String[] args) {
        Node f = new Node(1);
        Node s = new Node(2);
        f.setNext(s);
        s.setPrev(f);
        ReverseDoublyLinkedList service = new ReverseDoublyLinkedList();
        Node node = service.reverse(f);
    }

    public Node reverse(Node head){
        if (head == null) return null;
        Node resHead = head;
        head = head.getNext();
        resHead.setNext(null);
        while (head != null){
            head.setPrev(null);
            resHead.setPrev(head);
            head = head.getNext();
            resHead.getPrev().setNext(resHead);
            if (resHead.getPrev() != null)
                resHead = resHead.getPrev();
        }
        return resHead;
    }
}
