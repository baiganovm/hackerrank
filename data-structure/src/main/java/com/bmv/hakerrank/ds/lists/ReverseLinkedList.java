package com.bmv.hakerrank.ds.lists;

/**
 * You’re given the pointer to the head node of a linked list.
 * Change the next pointers of the nodes so that their order is reversed.
 * The head pointer given may be null meaning that the initial list is empty.
 */
public class ReverseLinkedList {
    public static void main(String[] args) {
        ReverseLinkedList list = new ReverseLinkedList();
        Node f = new Node(1);
        Node s = new Node(2);
        Node t = new Node(3);
        f.setNext(s);
        s.setNext(t);
        Node node = list.reverse(f);
        System.out.println(node == t);
    }

    public Node reverse(Node head){
        if (head == null) return null;
        Node it = head;
        Node next = it.getNext();
        Node tail = null;
        while (it != null){
            it.setNext(tail);
            tail = it;
            it = next;
            next = (next == null ? null : next.getNext());
        }
        return tail;
    }
}
