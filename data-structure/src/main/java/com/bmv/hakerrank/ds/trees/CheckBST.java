package com.bmv.hakerrank.ds.trees;

/**
 * For the purposes of this challenge, we define a binary search tree to be a binary tree with the following ordering requirements:
 * <p>
 * The data value of every node in a node's left subtree is less than the data value of that node.
 * The data value of every node in a node's right subtree is greater than the data value of that node.
 * Given the root node of a binary tree, can you determine if it's also a binary search tree?
 * <p>
 * Complete the function in your editor below, which has  parameter:
 * a pointer to the root of a binary tree.
 * It must return a boolean denoting whether or not the binary tree is a binary search tree.
 * You may have to write one or more helper functions to complete this challenge.
 */
public class CheckBST {
    public static void main(String[] args) {
        CheckBST checkBST = new CheckBST();

        Node root = new Node(3);

        Node left = new Node(2);
        left.setRight(new Node(4));
        root.setLeft(left);
        root.setRight(new Node(5));

        boolean b = checkBST.checkBST(root);
        System.out.println(b);
        root.getLeft().setRight(null);
        b = checkBST.checkBST(root);
        System.out.println(b);
    }

    public boolean checkBST(Node root) {
        return check(root, null, null);
    }

    private boolean check(Node root, Integer min, Integer max) {
        if (root == null) return true;

        if ((min != null && root.getData() <= min) || (max != null && root.getData() >= max)) return false;

        return check(root.getLeft(), min, root.getData()) && check(root.getRight(), root.getData(), max);
    }
}
