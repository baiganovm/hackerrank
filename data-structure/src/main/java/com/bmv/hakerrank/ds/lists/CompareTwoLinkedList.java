package com.bmv.hakerrank.ds.lists;

/**
 * You’re given the pointer to the head nodes of two linked lists.
 * Compare the data in the nodes of the linked lists to check if they are equal.
 * The lists are equal only if they have the same number of nodes and corresponding nodes contain the same data.
 * Either head pointer given may be null meaning that the corresponding list is empty.
 */
public class CompareTwoLinkedList {
    public static void main(String[] args) {
        CompareTwoLinkedList list = new CompareTwoLinkedList();
        System.out.println(list.compareLists(new Node(1), null));
        System.out.println(list.compareLists(new Node(1), new Node(1)));
    }

    public boolean compareLists(Node headA, Node headB){
        while (headA != null && headB != null){
            if (headA.getData() != headB.getData()) return false;

            headA = headA.getNext();
            headB = headB.getNext();
        }

        return headA == null && headB == null;
    }
}
