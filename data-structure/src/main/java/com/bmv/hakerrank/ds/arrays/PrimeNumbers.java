package com.bmv.hakerrank.ds.arrays;

import java.util.ArrayList;

public class PrimeNumbers {

    public static void main(String[] args) {
        PrimeNumbers numbers = new PrimeNumbers();
        System.out.println(numbers.sieve(7));
    }

    public ArrayList<Integer> sieve(int a) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        int[] temp = new int[a+1];
        for (int i = 0; i< temp.length; i++) {
            temp[i] = 1;
        }
        temp[0] = 0;
        temp[1] = 0;
        for (int i = 2; i <= a; i++){
            if (temp[i] == 1) {
                for (int j = 2; j * i <= a; j++) {
                    temp[i*j] = 0;
                }
            }
        }
        for (int i = 0; i < temp.length; i++) {
            if(temp[i] == 1){
                list.add(i);
            }
        }
        return list;
    }
}
