package com.bmv.hakerrank.ds.stacks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MaximumElement {

    private List<Long> values = new ArrayList<Long>();
    private List<Long> maxValues = new ArrayList<Long>();

    public static void main(String[] args) {
        MaximumElement element = new MaximumElement();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String line = reader.readLine();
            int n = Integer.parseInt(line);
            line = reader.readLine();
            while (!"".equals(line) && n-- > 0) {
                String[] split = line.split(" ");
                if (split.length > 1 && "1".equals(split[0])) {
                    element.push(Long.parseLong(split[1]));
                } else if ("2".equals(split[0])) {
                    element.pop();
                } else if ("3".equals(split[0])) {
                    System.out.println(element.maxValue());
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Long maxValue() {
        if (maxValues.isEmpty()) return null;
        return maxValues.get(maxValues.size() - 1);
    }

    public Long pop() {
        if (values.isEmpty()) return null;
        Long val = values.get(values.size() - 1);
        values.remove(values.size() - 1);
        maxValues.remove(maxValues.size() - 1);
        return val;
    }

    public void push(long value) {
        values.add(value);
        long max = Long.MIN_VALUE;
        if (!maxValues.isEmpty()){
            max = maxValues.get(maxValues.size() - 1);
        }
        if (value > max){
            maxValues.add(value);
        } else {
            maxValues.add(max);
        }
    }
}
