package com.bmv.hakerrank.ds.arrays;

/**
 * 2D Array - DS
 * Calculate the hourglass sum for every hourglass in A, then print the maximum hourglass sum.
 * -9 <=A[i][j] <= 9
 * 0 <= i, j <= 5
 *
 * Print the largest (maximum) hourglass sum found in A.
 */
public class Array2DDS {
    public static void main(String[] args) {
        int [][] a = new int[][]{
                {1, 1, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0},
                {0, 0, 2, 4, 4, 0},
                {0, 0, 0, 2, 0, 0},
                {0, 0, 1, 2, 4, 0}};
        Array2DDS array2DDS = new Array2DDS();
        System.out.println(array2DDS.maxHourglass(a));
    }

    public int maxHourglass(int[][] a){
        int max = Integer.MIN_VALUE;
        int current;
        for (int i = 1; i < a.length -1; i++){
            for (int j = 1; j < a[i].length -1; j++){
                current = a[i-1][j-1] + a[i-1][j] + a[i-1][j+1]
                        + a[i][j]
                        + a[i+1][j-1] + a[i+1][j] + a[i+1][j+1];
                if (current > max) max = current;
            }
        }
        return max;
    }
}
