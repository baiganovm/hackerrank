package com.bmv.hakerrank.ds.trees;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Created by Mikhail Baiganov on 20.06.17.
 */
public class SwapNodesAlgo {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        Node root = new Node(1);
        root.setLevel(0);
        Queue<Node> queue = new LinkedList<Node>();
        queue.add(root);
        sc.nextLine();
        while (!queue.isEmpty()) {
            Node node = queue.poll();
            String[] ss = sc.nextLine().split(" ");
            if (ss.length > 1) {
                if (!"".equals(ss[0]) && !"-1".equals(ss[0])) {
                    node.setLeft(new Node(Integer.valueOf(ss[0])));
                    node.getLeft().setLevel(node.getLevel() + 1);
                    queue.add(node.getLeft());
                }
                if (!"".equals(ss[1]) && !"-1".equals(ss[1])) {
                    node.setRight(new Node(Integer.valueOf(ss[1])));
                    node.getRight().setLevel(node.getLevel() + 1);
                    queue.add(node.getRight());
                }
            }
        }

        int t = sc.nextInt();

        for (int i = 0; i < t; i++) {
            int c = sc.nextInt();
            swap(root, c);
            printInTrav(root);
            System.out.println();
        }
    }

    private static void printInTrav(Node root) {
        if (root == null) return;
        printInTrav(root.getLeft());
        System.out.print(root.getData());
        System.out.print(" ");
        printInTrav(root.getRight());
    }

    private static void swap(Node root, int c) {
        Queue<Node> queue = new LinkedList<Node>();
        queue.add(root);
        while (!queue.isEmpty()) {
            Node node = queue.peek();
            if (node.getLevel() == c - 1) {
                break;
            }
            node = queue.poll();
            if (node.getLeft() != null) {
                queue.add(node.getLeft());
            }

            if (node.getRight() != null) {
                queue.add(node.getRight());
            }
        }
        int cur = 1;
        boolean isChanged = false;
        while (!queue.isEmpty()) {
            Node node = queue.poll();
            if (node.getLevel() + 1 == (cur * c)) {
                isChanged = true;
                Node temp = node.getLeft();
                node.setLeft(node.getRight());
                node.setRight(temp);
            }
            if (node.getLeft() != null)
                queue.add(node.getLeft());
            if (node.getRight() != null)
                queue.add(node.getRight());
            node = queue.peek();
            if (isChanged && node != null && node.getLevel() + 1 != cur * c) {
                cur = cur + 1;
                isChanged = false;
            }
        }
    }
}
