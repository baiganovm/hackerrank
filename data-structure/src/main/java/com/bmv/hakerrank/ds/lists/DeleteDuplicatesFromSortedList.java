package com.bmv.hakerrank.ds.lists;

/**
 * You're given the pointer to the head node of a sorted linked list, where the data in the nodes is in ascending order.
 * Delete as few nodes as possible so that the list does not contain any value more than once.
 * The given head pointer may be null indicating that the list is empty.

 */
public class DeleteDuplicatesFromSortedList {
    public static void main(String[] args) {
        DeleteDuplicatesFromSortedList instance = new DeleteDuplicatesFromSortedList();
        Node f = new Node(1);
        Node s = new Node(1);
        Node t = new Node(2);
        f.setNext(s);
        s.setNext(t);
        Node node = instance.removeDuplicates(f);

    }

    public Node removeDuplicates(Node head){
        if (head == null) return head;
        Node main = head;
        Node next = main.getNext();
        while (next != null){
            if (main.getData() == next.getData()){
                main.setNext(next.getNext());
                next.setNext(null);
                next = main.getNext();
            }
            else{
                main = main.getNext();
                next = next.getNext();
            }
        }
        return head;
    }
}
